const http = require('http');
const commandLineArgs = require('command-line-args');

let requestCount = 0;

const options = commandLineArgs([
  {
    name: 'port',
    alias: 'p',
    type: Number,
    defaultValue: 3000,
  },
]);

const server = http.createServer((req, res) => {
  requestCount += 1;
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(
    JSON.stringify({ message: 'Request handled successfully', requestCount }),
  );
});

const { port } = options;
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
